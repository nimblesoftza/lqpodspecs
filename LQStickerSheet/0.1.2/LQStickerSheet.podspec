Pod::Spec.new do |spec|

	spec.name			= 'LQStickerSheet'
	spec.version		= '0.1.2'
	spec.license		= { :type => 'MIT', :file => "LICENSE" }
	spec.homepage		= 'http://www.lifeq.com'
	spec.author		= 'Carien van Zyl' 
	spec.summary		= 'LQStickerSheet give user access to all controls on the LifeQ StickerSheet.'
	spec.source		= { :git => 'https://carien@bitbucket.org/nimblesoftza/lqstickersheet.git', :tag => '0.1.2'}
	spec.module_name 	= 'LQStickerSheet'
	spec.requires_arc = true
	
	spec.platform 				= :ios
	spec.ios.deployment_target = '10.0'

	spec.source_files  = 'LQStickerSheet/**/*.swift'
	spec.resources = 'LQStickerSheet/**/*.{png,jpeg,jpg,storyboard,xib}'

	spec.ios.framework	= 'UIKit'
  	spec.dependency 'Material'

  	spec.requires_arc = true
end
